# jsx-css-extract

It provides the following 'extract css` code action.

```
import React from 'react';
import { css } from '@emotion/core';

function PreciousComponent() {
  return (
    <div
      css={css\`
        display: none;
      \`}
    >
      asdf
    </div>
  );
}`;
```

After applying code action with moving cursor on css tagged literal.

```
import React from 'react';
import { css } from '@emotion/core';

const styles = {
  div: css`
    display: none;
  `
}

function PreciousComponent() {
  return (
    <div css={styles.div}>
      asdf
    </div>
  );
}`;
```

That's all.
