module.exports = {
  sourceType: 'unambiguous',
  presets: [
    '@babel/preset-typescript',
    [
      '@babel/preset-env',
      {
        targets: 'chrome 70',
      },
    ],
  ],
  plugins: [
    '@babel/plugin-proposal-nullish-coalescing-operator',
    '@babel/plugin-proposal-optional-chaining',
  ],
};
