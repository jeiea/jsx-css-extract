import { ExtractionResult } from './insert-css';
import { extractCssLiteralOnOffset } from './extract-css';

const source = `
import React from 'react';
import { css } from '@emotion/core';
  
function JustDummy() {
  return (
    <div
      css={css\`
        display: none;
      \`}
    >
      asdf
    </div>
  );
}`;

describe('given a file having no top styles variable', () => {
  const file = source;

  describe('when extract css literal', () => {
    let result: ExtractionResult | null;

    beforeAll(() => {
      result = extractCssLiteralOnOffset(file, file.indexOf('display'));
    });

    test('it should create top styles declaration', () => {
      if (!result) {
        fail();
      }
      const { insertionOffset, snippet } = result;
      const modified = `${file.slice(0, insertionOffset)}${snippet}${file.slice(
        insertionOffset,
        file.length,
      )}`;
      expect(modified).toMatchSnapshot();
    });
  });
});

describe('given a file having top styles variable', () => {
  const file = source.replace(/\nfun/, '\nconst styles = {\n// c\n};\n\nfun');

  describe('when extract css literal', () => {
    let result: ExtractionResult | null;

    beforeAll(() => {
      result = extractCssLiteralOnOffset(file, file.indexOf('display'));
    });

    test('it should create property of top styles', () => {
      if (!result) {
        fail();
      }
      const { insertionOffset, snippet } = result;
      const modified = `${file.slice(0, insertionOffset)}${snippet}${file.slice(
        insertionOffset,
        file.length,
      )}`;
      expect(modified).toMatchSnapshot();
    });
  });
});

describe('given a file having top styles property', () => {
  const file = source.replace(
    /\nfun/,
    `
const styles = {
  div: css\`
    width: 100%;
  \`
};

fun`,
  );

  describe('when extract css literal', () => {
    let result: ExtractionResult | null;

    beforeAll(() => {
      result = extractCssLiteralOnOffset(file, file.indexOf('display'));
    });

    test('it should create property of top styles', () => {
      if (!result) {
        fail();
      }
      const { insertionOffset, snippet } = result;
      const modified = `${file.slice(0, insertionOffset)}${snippet}${file.slice(
        insertionOffset,
        file.length,
      )}`;
      expect(modified).toMatchSnapshot();
    });
  });
});

describe('given file containing trailing comma', () => {
  const file = source.replace(
    /\nfun/,
    `
const styles = {
  div: css\`
    width: 100%;
  \`,
  // comment
};

fun`,
  );

  describe('when extract css literal', () => {
    let result: ExtractionResult | null;

    beforeAll(() => {
      result = extractCssLiteralOnOffset(file, file.indexOf('display'));
    });

    test('it should create property of top styles', () => {
      if (!result) {
        fail();
      }
      const { insertionOffset, snippet } = result;
      const modified = `${file.slice(0, insertionOffset)}${snippet}${file.slice(
        insertionOffset,
        file.length,
      )}`;
      expect(modified).toMatchSnapshot();
    });
  });
});
