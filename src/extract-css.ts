import { ExtractionResult, insertCss } from './insert-css';
import {
  File,
  JSXIdentifier,
  JSXOpeningElement,
  TaggedTemplateExpression,
  isIdentifier,
} from '@babel/types';
import { Node, NodePath, ParserOptions } from '@babel/core';
import { isPositioned } from './position';
import { parse } from '@babel/parser';
import traverse from '@babel/traverse';

const parsingOptions = {
  plugins: [
    'asyncGenerators',
    'classProperties',
    ['decorators', { decoratorsBeforeExport: true }],
    'jsx',
    'objectRestSpread',
    'optionalChaining',
    'typescript',
  ],
  sourceType: 'module',
} as ParserOptions;

export function extractCssLiteralOnOffset(
  text: string,
  offset: number,
): ExtractionResult | null {
  const ast = parse(text, parsingOptions);
  if (!ast) {
    return null;
  }

  const literal = findCssLiteralOnOffset(ast, offset);
  if (!literal) {
    return null;
  }

  const statements = ast.program.body;
  return insertCss(text, statements, literal.css.node, literal.tagName);
}

function findCssLiteralOnOffset(
  ast: File,
  offset: number,
): { tagName: string; css: NodePath<TaggedTemplateExpression> } | null {
  let css = null;
  traverse(ast, { TaggedTemplateExpression: findCssInJsxOnCursor });
  return css;

  function findCssInJsxOnCursor(
    path: NodePath<TaggedTemplateExpression>,
  ): void {
    if (!isContainingCursor(path.node)) {
      return;
    }
    const tagName = getJsxTagName(path);
    if (tagName) {
      css = { tagName, css: path };
    }
  }

  function isContainingCursor(node: Node): boolean {
    return isPositioned(node) && node.start <= offset && node.end >= offset;
  }
}

function getJsxTagName(path: NodePath<TaggedTemplateExpression>): string {
  if (!(isIdentifier(path.node.tag) && path.node.tag.name === 'css')) {
    return '';
  }
  const element = path.parentPath?.parentPath?.parent as JSXOpeningElement;
  const tagName = (element?.name as JSXIdentifier)?.name;
  return tagName || '';
}
