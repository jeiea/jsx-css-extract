import { ExtractionResult } from './insert-css';
import {
  Range,
  Selection,
  TextDocument,
  TextEditor,
  TextEditorEdit,
  window,
} from 'vscode';
import { extractCssLiteralOnOffset } from './extract-css';

export function isCursorOnCssLiteral(
  document: TextDocument,
  range: Range | Selection,
): boolean {
  return !!extractCssLiteralOnDocument(document, range);
}

export function extractCssToTopStyle(): void {
  const editor = window.activeTextEditor;
  if (!editor) {
    return;
  }

  const { document } = editor;
  const range = editor.selection;
  const result = extractCssLiteralOnDocument(document, range);
  if (!result) {
    return;
  }

  refactor(result, editor);
  return;

  function refactor(result: ExtractionResult, editor: TextEditor): void {
    const { document } = editor;
    const cssStart = document.positionAt(result.css.start);
    const cssEnd = document.positionAt(result.css.end);
    const inlineCss = new Range(cssStart, cssEnd);
    const insertionPosition = document.positionAt(result.insertionOffset);
    editor.edit((edit: TextEditorEdit) => {
      edit.replace(inlineCss, `styles.${result.key}`);
      edit.replace(insertionPosition, result.snippet);
    });
  }
}

function extractCssLiteralOnDocument(
  document: TextDocument,
  range: Range | Selection,
): ExtractionResult | null {
  const text = document.getText();
  const offset = document.offsetAt(range.start);
  return extractCssLiteralOnOffset(text, offset);
}
