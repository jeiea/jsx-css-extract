import {
  Node,
  ObjectExpression,
  ObjectMethod,
  ObjectProperty,
  Statement,
  TaggedTemplateExpression,
  VariableDeclarator,
  isIdentifier,
  isImportDeclaration,
  isObjectExpression,
  isObjectMethod,
  isObjectProperty,
} from '@babel/types';
import { Positioned, isPositioned } from './position';

export type ExtractionResult = {
  css: Positioned;
  key: string;
  snippet: string;
  insertionOffset: number;
};

export function insertCss(
  text: string,
  statements: Statement[],
  css: TaggedTemplateExpression,
  key: string,
): ExtractionResult | null {
  if (!isPositioned(css)) {
    return null;
  }

  const topStyles = findTopStylesVariable(statements);
  if (isPositioned(topStyles)) {
    return insertCssToTopStyles(topStyles, css);
  } else {
    return createStylesIncludingCss(css);
  }

  function createStylesIncludingCss(css: Positioned): ExtractionResult {
    const insertionOffset = Math.max(0, getFirstNonImportOffset(statements));
    const property = getPropertyString(key, css);
    const snippet = `\nconst styles = {${property}};\n`;
    return { css, key, insertionOffset, snippet };
  }

  function insertCssToTopStyles(
    topStyles: ObjectExpression & Positioned,
    css: Node & Positioned,
  ): ExtractionResult {
    const properties = getNamedProperties(topStyles);
    const key = getConflictFreeKey(properties);
    const lastProperty = properties[properties.length - 1];
    const snippet = getPropertyString(key, css);
    if (lastProperty?.end) {
      return {
        css,
        key,
        insertionOffset: lastProperty.end,
        snippet: `,${snippet}`,
      };
    } else {
      return { css, key, insertionOffset: topStyles.end - 1, snippet };
    }
  }

  function getPropertyString(key: string, css: Positioned): string {
    return `\n  ${key}: ${getNodeString(css)}\n`;
  }

  type NamedProperty = ObjectProperty | ObjectMethod;

  function getNamedProperties(expression: ObjectExpression): NamedProperty[] {
    return expression.properties.filter(isNamedProperty);

    function isNamedProperty(property: object): property is NamedProperty {
      return isObjectProperty(property) || isObjectMethod(property);
    }
  }

  function getConflictFreeKey(properties: NamedProperty[]): string {
    const keys = getPropertyKeys(properties);
    return augmentNumberForId(keys, key);

    function getPropertyKeys(properties: NamedProperty[]): string[] {
      return properties.map((x) => x.key.name);
    }

    function augmentNumberForId(keys: string[], key: string): string {
      let id = key;
      for (let num = 2; keys.includes(id); num++) {
        id = `${id}${num}`;
      }
      return id;
    }
  }

  function getFirstNonImportOffset(statements: Statement[]): number {
    if (statements.length < 1) {
      return 0;
    }

    const firstNonImport = statements.find((x) => !isImportDeclaration(x));
    if (firstNonImport) {
      return firstNonImport.start ? Math.max(0, firstNonImport.start - 1) : -1;
    } else {
      const lastStatement = statements[statements.length - 1];
      return lastStatement.end ? lastStatement.end + 1 : -1;
    }
  }

  function getNodeString(node: Positioned): string {
    return text.slice(node.start, node.end);
  }

  function findTopStylesVariable(
    statements: Statement[],
  ): ObjectExpression | null {
    return statements.map(getTopStylesVariable).filter((x) => x)[0] || null;
  }

  function getTopStylesVariable(statement: Statement): ObjectExpression | null {
    if (statement.type !== 'VariableDeclaration') {
      return null;
    }
    return findStylesVariable(statement.declarations);
  }

  function findStylesVariable(
    declarators: VariableDeclarator[],
  ): ObjectExpression | null {
    for (const { id, init } of declarators) {
      if (
        isIdentifier(id) &&
        id.name === 'styles' &&
        isObjectExpression(init)
      ) {
        return init;
      }
    }
    return null;
  }
}
