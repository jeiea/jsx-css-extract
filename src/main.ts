import {
  CancellationToken,
  CodeActionContext,
  CodeActionProvider,
  Command,
  ExtensionContext,
  Range,
  Selection,
  TextDocument,
  commands,
  languages,
} from 'vscode';
import {
  extractCssToTopStyle,
  isCursorOnCssLiteral,
} from './extract-document-css';

const extractToTopStyleCommand = {
  id: 'extension.reactExtract.extractToTopStyles',
  title: 'Extract css to style',
  run: extractCssToTopStyle,
};

const extractActionProvider = ((): CodeActionProvider => {
  return {
    provideCodeActions,
  };

  function provideCodeActions(
    document: TextDocument,
    range: Range | Selection,
    _context: CodeActionContext,
    _token: CancellationToken,
  ): Command[] {
    try {
      return provideExecutableActions(document, range);
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  function provideExecutableActions(
    document: TextDocument,
    range: Range | Selection,
  ): Command[] {
    if (isCursorOnCssLiteral(document, range)) {
      return [
        {
          command: extractToTopStyleCommand.id,
          title: extractToTopStyleCommand.title,
        },
      ];
    }
    return [];
  }
})();

export async function activate(context: ExtensionContext): Promise<void> {
  context.subscriptions.push(
    languages.registerCodeActionsProvider(
      [{ language: 'javascriptreact' }, { language: 'typescriptreact' }],
      extractActionProvider,
    ),
    commands.registerCommand(
      extractToTopStyleCommand.id,
      extractToTopStyleCommand.run,
    ),
  );
}

export function deactivate(): void {
  // TODO
}
