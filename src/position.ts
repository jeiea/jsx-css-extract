import { Node } from '@babel/types';

export type Positioned = { start: number; end: number };

export function isPositioned(node?: Node | null): node is Node & Positioned {
  const { start, end } = (node as { start: number; end: number } | null) || {};
  return !(start == null || end == null);
}
