const webpack = require('webpack');
const path = require('path');
const srcDir = path.join(__dirname, '../src');

/**@type {import('webpack').Configuration}*/
module.exports = {
  entry: {
    extension: path.join(srcDir, 'main.ts'),
  },
  output: {
    path: path.resolve(__dirname, '..'),
    filename: '[name].js',
    libraryTarget: 'commonjs2',
  },
  optimization: {
    splitChunks: {},
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: {
          loader: 'babel-loader',
          options: require('../babel.config'),
        },
        include: path.resolve(__dirname, '../src'),
        exclude: /node_modules/,
      },
    ],
  },
  target: 'node',
  externals: {
    vscode: 'commonjs vscode',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx'],
  },
};
