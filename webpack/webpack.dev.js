const merge = require('webpack-merge');
const common = require('./webpack.common.js');

/**@type {import('webpack').Configuration}*/
module.exports = merge(common, {
  devtool: 'eval-source-map',
  output: {
    devtoolModuleFilenameTemplate: '[absolute-resource-path]',
  },
  mode: 'development',
});
